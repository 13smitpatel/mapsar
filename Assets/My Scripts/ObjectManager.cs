using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ObjectManager : MonoBehaviour
{
    [SerializeField] private ARPlaneManager planeManager;
    [SerializeField] private ARRaycastManager raycastManager;
    [SerializeField] private GameObject Cube;
    private bool objctPlaced;

    private List<ARRaycastHit> aRRaycastHits = new List<ARRaycastHit>();
    
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if(raycastManager.Raycast(touch.position, aRRaycastHits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon) && !objctPlaced)
            {
                Instantiate(Cube, aRRaycastHits[0].pose.position ,Cube.transform.rotation);
                objctPlaced = true;
                planeManager.SetTrackablesActive(false);
                planeManager.enabled = false;

                //collectiong information behaviour componenets
                Gaze.Instance._CollectInfoBehaviours();
            }


        }
       
    }
}
