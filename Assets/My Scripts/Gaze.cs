using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UIElements;
using System;

public class Gaze : MonoBehaviour
{
    public static Gaze Instance { get; private set; }

    public List<InfoBehaviour> Infos = new List<InfoBehaviour>();
    //public List<GameObject> States = new List<GameObject>();

    //// info to state map 
    //public Dictionary<string,InfoBehaviour> infos = new Dictionary<string,InfoBehaviour>(); 

    private void Start()
    {
        Infos = FindObjectsOfType<InfoBehaviour>().ToList();
        if (Instance == null)
        {
            Instance = this;
        }
        //foreach(GameObject state in States)
        //{
        //    infos.Add(state.name,state.GetComponent<InfoBehaviour>());
        //    //Debug.Log(state.name); 
        //}
        Debug.Log(Infos.Count);
    }
    public void _CollectInfoBehaviours()
    {
        Infos = FindObjectsOfType<InfoBehaviour>().ToList();
    }
    void Update()
    {
        if(Infos.Count == 0)
        {
            FindObjectsOfType<InfoBehaviour>().ToList();
            
        }
        if(Physics.Raycast(transform.position,transform.forward,out RaycastHit hit,100f)) 
        {
            if (hit.collider.gameObject.CompareTag("HasInfo"))
            {
                //open info
                _OpenInfo(hit.collider.gameObject.GetComponent<InfoBehaviour>());
                //Debug.Log("Ray is hitting the cube");
            }
            else
            {
                _CloseAll();
            }

        }
        else
        {
            _CloseAll();
        }
  
    }

    private void _OpenInfo(InfoBehaviour _desiredInfo)
    {
        foreach(InfoBehaviour info in Infos)
        {
            if(info == _desiredInfo)
            {
                info._ChangeColor();
                info._OpenInfo();
               
            }
            else
            {
                info._CloseInfo();
                info._RestoreColor();
            }
        }
    }

    private void _CloseAll()
    {
        foreach (InfoBehaviour info in Infos)
        {
            info._RestoreColor();
            info._CloseInfo();
        }
    }
}
