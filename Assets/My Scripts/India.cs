using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class India : MonoBehaviour
{
    //Static instasnt
    public static India Instance { get; private set; }

    // Start is called before the first frame update
    public List<Transform> States = new List<Transform>();

    // info to state map 
    public Dictionary<string, InfoBehaviour> StateInfos = new Dictionary<string, InfoBehaviour>();

    public List<InfoBehaviour> Infos = new List<InfoBehaviour>();

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        _GatherInfo();
    }

    public void _GatherInfo()
    {
        foreach (Transform state in States)
        {
            if(!StateInfos.ContainsKey(state.name))
                StateInfos.Add(state.name, state.GetComponent<InfoBehaviour>());
            //Debug.Log(state.name); 
        }
        Infos = this.gameObject.GetComponentsInChildren<InfoBehaviour>().ToList<InfoBehaviour>();
        Debug.Log(Infos.Count);
    }

    public void _AssignGeneralInfo()
    {
        foreach(var iterator in StateInfos)
        {
            Debug.Log(iterator.Key);
            iterator.Value.infoText.text = "Name : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].name + "\n" +
                                                   "Capital : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].generalInfo.capital + "\n" +
                                                   "LanguageSpoken : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].generalInfo.languageSpoken;
        }
    }
    public void _AssignPoliticalInfo()
    {
        foreach (var iterator in StateInfos)
        {
            iterator.Value.infoText.text = "Name : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].name + "\n" +
                                                   "Chief Minister : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].politicalInfo.chiefMinister + "\n" +
                                                   "Political Party : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].politicalInfo.politicalParty;
        }
    }

    public void _AssignGeographicalInfo()
    {
        foreach (var iterator in StateInfos)
        {
            iterator.Value.infoText.text = "Name : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].name + "\n" +
                                                   "Districts : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].geographicalInfo.districts + "\n" +
                                                   "Rivers : " + JsonReaderTwo.Instance.keyValuePairs[iterator.Key].geographicalInfo.rivers;
        }
    }
    

}
