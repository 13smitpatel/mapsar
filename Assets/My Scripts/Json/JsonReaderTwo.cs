using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JsonReaderTwo : MonoBehaviour
{

    public TextAsset JsonFile;
    [SerializeField] private string JsonFilePath;
    public static JsonReaderTwo Instance { get; private set; }
    public States data { get; private set; }

    public Dictionary<string, Data.State> keyValuePairs = new Dictionary<string, Data.State>();// { get; private set; }

    void Start()
    {
        if (Instance == null)
            Instance = this;

        data = JsonConvert.DeserializeObject<States>(JsonFile.text);

        Debug.Log("Name : " + data.states[25].name + " Ceif Minister : " + data.states[25].politicalInfo.chiefMinister + " Political Party : " + data.states[25].politicalInfo.politicalParty);
        
        foreach (Data.State state in data.states)
        {
            keyValuePairs.Add(state.name, state);

            //Debug.Log("Name : " + state.name + " Ceif Minister : " + state.politicalInfo.chiefMinister + " Political Party : " + state.politicalInfo.politicalParty);
        }

    }
}
