using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data{
    public class GeneralInfo
    {
        public string capital { get; set; }
        public string population { get; set; }
        public string languageSpoken { get; set; }
    }

    public class GeographicalInfo
    {
        public int districts { get; set; }
        public string rivers { get; set; }
    }

    public class PoliticalInfo
    {
        public string chiefMinister { get; set; }
        public string politicalParty { get; set; }
    }
    public class State
    {
        //public int id { get; set; }
        public string name { get; set; }
        public GeographicalInfo geographicalInfo { get; set; }
        public GeneralInfo generalInfo { get; set; }

        public PoliticalInfo politicalInfo { get; set; }
    }

}

[System.Serializable]
public class States
{
    public List<Data.State> states = new List<Data.State>();
}

