using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DropdownBehaviour : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown m_Dropdown;

    //[SerializeField]
    //private GameObject GujratState;

    void Start()
    {
        if(InfoBehaviour.Instance == null)
        {
            Debug.Log("infobehaviour null");
        }
        if (JsonReaderTwo.Instance)
        {
            Debug.Log("json instance null");
        }
        

        m_Dropdown.onValueChanged.AddListener(delegate { 
            _OnDropdownValueChange(m_Dropdown); 
        });
    }


    public void _OnDropdownValueChange(TMP_Dropdown dropdown)
    {
        if (InfoBehaviour.Instance == null)
        {
            Debug.Log("infobehaviour null");
        }

        switch (dropdown.value)
        {
            case 0:
                InfoBehaviour.Instance.infoText.text = "";
                break;
            case 1:
                India.Instance._AssignGeneralInfo();
                break;
            case 2:
                India.Instance._AssignGeographicalInfo();
                break;
            case 3:
                India.Instance._AssignPoliticalInfo();
                break;
                        
        }
       
    }

   
}
