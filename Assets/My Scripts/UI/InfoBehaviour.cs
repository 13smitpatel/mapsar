using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfoBehaviour : MonoBehaviour
{
    public static InfoBehaviour Instance { get; private set; }

    const float SPEED = 6f;
    [SerializeField]
    private Transform SectionTransform;
    

    private Vector3 DesiredScale = Vector3.zero;
    private Vector3 CurrentSectionPosition;

    public TMP_Text infoText;
    private void Start()
    {
        SectionTransform = transform.GetChild(0);
        infoText = GetComponentInChildren<TMP_Text>();
        if(Instance == null)
        {
            Instance = this;
        }
    }

    void Update()
    {
        CurrentSectionPosition = SectionTransform.localScale;
        SectionTransform.localScale = Vector3.Lerp(CurrentSectionPosition, DesiredScale, Time.deltaTime * SPEED); 
    }


    public void _OpenInfo()
    {
        DesiredScale = Vector3.one;
    }

    public void _CloseInfo()
    {
        DesiredScale = Vector3.zero;
    }

    public void _ChangeColor()
    {
        transform.GetComponent<MeshRenderer>().material.color = Color.red;
    }

    public void _RestoreColor()
    {
        transform.GetComponent<MeshRenderer>().material.color = Color.black;
    }
}
