using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class FollowCam : MonoBehaviour
{
    //private Transform MainCamera;

    //private Vector3 TargetAngle = Vector3.zero;
    //// Update is called once per frame

   
    //void Update()
    //{
    //    transform.LookAt(MainCamera);
    //    TargetAngle = transform.eulerAngles;
    //    TargetAngle.x = 0f;
    //    TargetAngle.z = 0f;
    //    transform.localEulerAngles = TargetAngle;
    //}

    // The camera to follow
    [SerializeField] private Transform cameraTransform;

    private void Start()
    {
        cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        // Get the camera's rotation in Euler angles
        Vector3 cameraEulerAngles = cameraTransform.rotation.eulerAngles;

        // Set the object's rotation to match the camera's x and y rotation, but keep the initial z rotation
        transform.rotation = Quaternion.Euler(cameraEulerAngles.x, cameraEulerAngles.y, cameraEulerAngles.z);
    }

}
